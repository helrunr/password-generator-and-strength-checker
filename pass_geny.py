import random
import passwordmeter

class Gen_Pass:
    def __init__(self, pwd_len):
        self.pwd_len = pwd_len
    
    def generate(self):
        strng = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
        pss_wrd = "".join(random.sample(strng, self.pwd_len))

        return pss_wrd
    
    def analyze_strength(self, func):
        str_test = passwordmeter.test(func)
        return str_test


if __name__=='__main__':
    print("Enter the length of the password to generate: ", end='')
    user_length = int(input())
    
    pwrd = Gen_Pass(user_length)
    pwrd_str = pwrd.analyze_strength(pwrd.generate())

    while pwrd_str[0] < 0.9:
        pwrd = Gen_Pass(user_length)
        pwrd_str = pwrd.analyze_strength(pwrd.generate())
    else:
        print(pwrd.generate())