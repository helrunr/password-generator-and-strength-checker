# Password Generator and Strength Checker
Generates a random password based on user defined length. Strength of the password is tested and must pass a
certain threshold before the password is returned.
